from dotenv import load_dotenv
import mysql.connector as connection
from sklearn.feature_extraction.text import TfidfVectorizer
import os, re

# Source: https://www.kaggle.com/rowhitswami/keywords-extraction-using-tf-idf-method

PUNCTUATION = """!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~""" 
TOP_K_KEYWORDS = 5 # top k number of keywords to retrieve in a ranked document

all_ocupations_descriptions = []
vectorizer = None
feature_names = None

# Gets the top five keywords for a occupation description
# using a trained vectorizer with all the occupations available 
def getKeywordsByInput(db, user_challenge_description):
  # Set this variables at global so we can modify them
  global all_ocupations_descriptions, vectorizer, feature_names

  # Only fetch the full occupation description once
  if not all_ocupations_descriptions:
    query_occupations = "SELECT occupation_description FROM occupations"
    cursor = db.cursor()
    cursor.execute(query_occupations)
    occupations_tuple = cursor.fetchall()
    for occupation in occupations_tuple:
      all_ocupations_descriptions.append(clean_text(occupation[0]))

    # Initializing TF-IDF Vectorizer with stopwords
    # This process is calculated once
    vectorizer = TfidfVectorizer(stop_words="english", smooth_idf=True, use_idf=True)
    vectorizer.fit_transform(all_ocupations_descriptions)
    feature_names = vectorizer.get_feature_names()
  
  top_keywords = get_keywords(vectorizer, feature_names, user_challenge_description)
  # print("\n> Top keywords for", user_challenge_description, ":", top_keywords)
  return top_keywords


def clean_text(text):
    """Doc cleaning"""
    # Lowering text
    text = text.lower()
    # Removing punctuation
    text = "".join([c for c in text if c not in PUNCTUATION])
    # Removing whitespace and newlines
    text = re.sub('\s+',' ',text)
    
    return text

def get_keywords(vectorizer, feature_names, doc):
    """Return top k keywords from a doc using TF-IDF method"""
    # Generate tf-idf for the given document
    tf_idf_vector = vectorizer.transform([doc])
    
    # Sort the tf-idf vectors by descending order of scores
    sorted_items = sort_coo(tf_idf_vector.tocoo())

    # Extract only TOP_K_KEYWORDS
    keywords = extract_topn_from_vector(feature_names, sorted_items, TOP_K_KEYWORDS)
    
    return list(keywords.keys())

def sort_coo(coo_matrix):
    """Sort a dict with highest score"""
    tuples = zip(coo_matrix.col, coo_matrix.data)
    return sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)

def extract_topn_from_vector(feature_names, sorted_items, topn=10):
    """get the feature names and tf-idf score of top n items"""
    # Use only topn items from vector
    sorted_items = sorted_items[:topn]
    score_vals = []
    feature_vals = []
    
    # Word index and corresponding tf-idf score
    for idx, score in sorted_items:
        # Keep track of feature name and its corresponding score
        score_vals.append(round(score, 3))
        feature_vals.append(feature_names[idx])

    # Create a tuples of feature, score
    results= {}
    for idx in range(len(feature_vals)):
        results[feature_vals[idx]] = score_vals[idx]

    return results


def main():
  # Load .env variables
  load_dotenv()
  try:
    # Create a MySQL connection
    db = connection.connect(host = os.getenv('MYSQL_HOST'), user = os.getenv('MYSQL_USER'), port = os.getenv('MYSQL_PORT'), 
      passwd =  os.getenv('MYSQL_PASSWORD'), database = os.getenv('MYSQL_DATABASE'), use_pure = True)

    # Get all the users that have set at least one job challenge
    # We will extract the keywords from the job challenges of these users
    query_users_with_challenges = "SELECT DISTINCT user_id FROM users_occupations"
    cursor = db.cursor()
    cursor.execute(query_users_with_challenges)

    # Build a dict with the user_id as the key and an array of keywords as the value
    # Eg. { "user_id": 132, "keywords": ["wood", "3d"] }
    users_keywords = []

    # Iterate for every user available
    users_with_challenges_tuple = cursor.fetchall()
    for user_id in users_with_challenges_tuple:
      user_id = str(user_id[0])
      print("\nChecking user", user_id)

      # Get the job challenges with its description for the user
      query_user_challenges = "SELECT DISTINCT occupation_description FROM users_occupations INNER JOIN occupations ON users_occupations.occupation_id = occupations.occupation_id WHERE user_id = " + user_id
      cursor = db.cursor()
      cursor.execute(query_user_challenges)
      # The database returns a tuple   
      user_challenges_tuple = cursor.fetchall()

      # Preapare a list that will contain all the keywords of the user
      keywords = []
      for user_challenge in user_challenges_tuple:
        # The getKeywordsByInput function extracts the keywords using the TF-IDF NLP algorithm
        # We merge all the keywords found in the occupations for the given user
        keywords += getKeywordsByInput(db, user_challenge[0])
    
      # Remove duplicated keywords
      keywords = list(set(keywords))
      # Append the user_id and the list of keywords in the dictionary that we previously created
      users_keywords.append({ "user_id": user_id, "keywords": keywords })


    # Delete previously saved recommendations
    cursor.execute("DELETE FROM candidates_recommendations WHERE type = 1")

    # Now we will extract all the training providers and their curses
    # We will try to match the courses keywords with the keywords from the user's occupations extracted before
    query_trainers = "SELECT user_id FROM users WHERE role_id = 2"
    cursor = db.cursor()
    cursor.execute(query_trainers)

    trainers_tuple = cursor.fetchall()
    # Iterate over all the training providers
    for trainer in trainers_tuple:
      trainer_id = str(trainer[0])
      # Get all the courses from the training provider
      query_courses = "SELECT course_id, LOWER(keywords) AS keywords FROM courses WHERE user_id = " + trainer_id
      cursor = db.cursor()
      cursor.execute(query_courses)

      # Iterate over all the courses published by the training provider
      courses_tuple = cursor.fetchall()
      for course in courses_tuple:
        course_id = course[0]

        # This list will contain the user_ids and the number of matches between the two lists of keywords
        user_trainer_matches = []

        # Skip courses without keywords
        if not course[1]: continue

        # Transform the keyword string into a list of keywords
        course_keywords = [x.strip() for x in course[1].split(',')]

        # Check if the course keywords contains the keywords of any user
        for user in users_keywords:
          users_keywords_set = set(user["keywords"])
          course_keywords_set = set(course_keywords)
          total_count = sum(1 for x in course_keywords_set if x in users_keywords_set)
          
          # If more than one keywords matches, save the user in the list
          if total_count > 0:
            user_trainer_matches.append({ "user_id": user["user_id"], "total_count": total_count })

      
        # Save the top users by matches by course in the db
        for user_trainer_match in user_trainer_matches:
          print("Inserting trainer", trainer_id, "candidate", user_trainer_match['user_id'], "course", course_id, "matches", user_trainer_match['total_count'])
          cursor.execute("INSERT INTO candidates_recommendations (user_id, candidate_id, course_id, score, type) VALUES(%s, %s, %s, %s, 1)",
          (trainer_id, int(user_trainer_match['user_id']), course_id, user_trainer_match['total_count']))

    # Execute all the SQL write queries
    db.commit()
  except Exception as e:
    # Print any error occured
    print(str(e))
  finally:
    # Always close the db connection
    db.close()

if __name__ == "__main__":
  main()
