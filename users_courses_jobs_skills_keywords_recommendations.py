from dotenv import load_dotenv
import mysql.connector as connection
from sklearn.feature_extraction.text import TfidfVectorizer
import os, re

# Source: https://www.kaggle.com/rowhitswami/keywords-extraction-using-tf-idf-method

PUNCTUATION = """!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~""" 
TOP_K_KEYWORDS = 5 # top k number of keywords to retrieve in a ranked document

all_skills_descriptions = []
vectorizer = None
feature_names = None

# Gets the top five keywords for a skill description
# using a trained vectorizer with all the skills available 
def getKeywordsByInput(db, skill_description):
  # Set this variables at global so we can modify them
  global all_skills_descriptions, vectorizer, feature_names

  # Only fetch the full skills description once
  if not all_skills_descriptions:
    query_skills = "SELECT skill_description FROM skills"
    cursor = db.cursor()
    cursor.execute(query_skills)
    skills_tuple = cursor.fetchall()
    for skill in skills_tuple:
      all_skills_descriptions.append(clean_text(skill[0]))

    # Initializing TF-IDF Vectorizer with stopwords
    # This process is calculated once
    vectorizer = TfidfVectorizer(stop_words="english", smooth_idf=True, use_idf=True)
    vectorizer.fit_transform(all_skills_descriptions)
    feature_names = vectorizer.get_feature_names()
  
  top_keywords = get_keywords(vectorizer, feature_names, skill_description)
  # print("\n> Top keywords for", skill_description, ":", top_keywords)
  return top_keywords

def clean_text(text):
    """Doc cleaning"""
    # Lowering text
    text = text.lower()
    # Removing punctuation
    text = "".join([c for c in text if c not in PUNCTUATION])
    # Removing whitespace and newlines
    text = re.sub('\s+',' ',text)
    
    return text

def get_keywords(vectorizer, feature_names, doc):
    """Return top k keywords from a doc using TF-IDF method"""
    # Generate tf-idf for the given document
    tf_idf_vector = vectorizer.transform([doc])
    
    # Sort the tf-idf vectors by descending order of scores
    sorted_items = sort_coo(tf_idf_vector.tocoo())

    # Extract only TOP_K_KEYWORDS
    keywords = extract_topn_from_vector(feature_names, sorted_items, TOP_K_KEYWORDS)
    
    return list(keywords.keys())

def sort_coo(coo_matrix):
    """Sort a dict with highest score"""
    tuples = zip(coo_matrix.col, coo_matrix.data)
    return sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)

def extract_topn_from_vector(feature_names, sorted_items, topn=10):
    """get the feature names and tf-idf score of top n items"""
    # Use only topn items from vector
    sorted_items = sorted_items[:topn]
    score_vals = []
    feature_vals = []
    
    # Word index and corresponding tf-idf score
    for idx, score in sorted_items:
        # Keep track of feature name and its corresponding score
        score_vals.append(round(score, 3))
        feature_vals.append(feature_names[idx])

    # Create a tuples of feature, score
    results= {}
    for idx in range(len(feature_vals)):
        results[feature_vals[idx]] = score_vals[idx]

    return results


def main():
  # Load .env variables
  load_dotenv()
  try:
    # Create a MySQL connection
    db = connection.connect(host = os.getenv('MYSQL_HOST'), user = os.getenv('MYSQL_USER'), port = os.getenv('MYSQL_PORT'), 
      passwd =  os.getenv('MYSQL_PASSWORD'), database = os.getenv('MYSQL_DATABASE'), use_pure = True)

    # Get all the skills description from the jobs available in the database
    # We will extract the top keywords from these descriptions
    query_skills_in_jobs = "SELECT skill_description FROM jobs_skills INNER JOIN skills ON jobs_skills.skill_id = skills.skill_id"
    cursor = db.cursor()
    cursor.execute(query_skills_in_jobs)

    # The top keywords available in the job skills will be saved here
    jobs_skills_keywords = []

    # Iterate for every job desciption available
    skills_in_jobs_tuple = cursor.fetchall()
    for skill in skills_in_jobs_tuple:
      # The getKeywordsByInput function extracts the keywords using the TF-IDF NLP algorithm
      # We merge all the keywords found in the skills
      jobs_skills_keywords += getKeywordsByInput(db, skill[0])
    
    # Remove duplicated keywords
    jobs_skills_keywords = list(set(jobs_skills_keywords))
    print("\nJobs keywords by skills:", jobs_skills_keywords)


    # Delete previously saved recommendations
    cursor.execute("DELETE FROM candidates_recommendations WHERE type = 2")

    print("\nTrying to find courses that matches these keywords...")

    # Get all the courses and their keywords
    # We will try to find courses with similar keywords as the jobs keywords list
    query_courses = "SELECT course_id, LOWER(keywords) AS keywords FROM courses"
    cursor = db.cursor()
    cursor.execute(query_courses)

    # Iterate over all the courses
    courses_tuple = cursor.fetchall()
    for course in courses_tuple:
      course_id = course[0]

      # Skip courses without keywords
      if not course[1]: continue

      # Transform the keyword string into a list of keywords
      course_keywords = [x.strip() for x in course[1].split(',')]

      jobs_keywords_set = set(jobs_skills_keywords)
      course_keywords_set = set(course_keywords)
      total_count = sum(1 for x in course_keywords_set if x in jobs_keywords_set)
          
      # If more than one keywords matches, save the course in the database
      if total_count > 0:
        # Save the top courses by matches by course in the db.
        # We reuse the same table as the candidates recommendations but with a 0 as the user_id and candidate_id
        print("Inserting course", course_id, "matches", total_count)
        cursor.execute("INSERT INTO candidates_recommendations (user_id, candidate_id, course_id, score, type) VALUES(0, 0, %s, %s, 2)",
         (course_id, total_count))

    # Execute all the SQL write queries
    db.commit()
  except Exception as e:
    # Print any error occured
    print(str(e))
  finally:
    # Always close the db connection
    db.close()

if __name__ == "__main__":
  main()
