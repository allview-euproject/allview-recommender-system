# ALLVIEW RECOMMENDER SYSTEM
### This repo contains the recommender system of the ALLVIEW Platform, written in Python 3

<br>

## Documentation
- [Surprise](https://surprise.readthedocs.io/)

<br>

## Installation
Python 3.8.5 or higher with Pip must be installed. You can download the latest stable release for your operating system [here](https://www.python.org/downloads/).

Install all the project dependencies
```
pip install -r requirements.txt
```

<br>

## Configuration
Copy the `env.example` file to `.env` and set your custom environment variables.
```
cp env.example .env
nano .env
```

<br>

## Run recommendations
You should configure a Cron service that runs the scripts every hour.
```
0 * * * * cd /home/allview/allview-recommender-system/ && for script in *.py; do python3 $script"; done
```

