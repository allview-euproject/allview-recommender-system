import mysql.connector as connection
import pandas as pd
from dotenv import load_dotenv
import numpy as np
import os
from surprise import Dataset
from surprise import Reader
from surprise import SVD, SVDpp, SlopeOne, NMF, NormalPredictor, KNNBaseline, KNNBasic, KNNWithMeans, KNNWithZScore, BaselineOnly, CoClustering
from surprise.model_selection import cross_validate, train_test_split
from surprise import accuracy

# Surprise Docs: https://surprise.readthedocs.io/
# Extra source 1: https://predictivehacks.com/how-to-run-recommender-systems-in-python/
# Extra source 2: https://towardsdatascience.com/building-and-testing-recommender-systems-with-surprise-step-by-step-d4ba702ef80b

def get_best_algorithm(data):
  print('\nBenchmarking algorithms...\n')
  # Cross-validate all the available algorithms with the current dataset
  benchmark = []
  # Iterate over all algorithms
  for algorithm in [SVD(), SVDpp(), SlopeOne(), NMF(), NormalPredictor(), KNNBaseline(), KNNBasic(), KNNWithMeans(), KNNWithZScore(), BaselineOnly(), CoClustering()]:
    # Perform cross validation
    results = cross_validate(algorithm, data, measures = ['RMSE'], cv = 3, verbose = False)
    # Get results & append algorithm name
    tmp = pd.DataFrame.from_dict(results).mean(axis=0)
    tmp = tmp.append(pd.Series([str(algorithm).split(' ')[0].split('.')[-1]], index=['Algorithm']))
    benchmark.append(tmp)
  
  print('\nBenchmark results (the lower RMSE the better)\n')
  df_benchmark = pd.DataFrame(benchmark).set_index('Algorithm').sort_values('test_rmse')
  print(df_benchmark)

  # Load the best algorithm by the name
  if df_benchmark.index[0] == 'SVD': algo = SVD()
  if df_benchmark.index[0] == 'SVDpp': algo = SVDpp()
  if df_benchmark.index[0] == 'SlopeOne': algo = SlopeOne()
  if df_benchmark.index[0] == 'NMF': algo = NMF()
  if df_benchmark.index[0] == 'NormalPredictor': algo = NormalPredictor()
  if df_benchmark.index[0] == 'KNNBaseline': algo = KNNBaseline()
  if df_benchmark.index[0] == 'KNNBasic': algo = KNNBasic()
  if df_benchmark.index[0] == 'KNNWithMeans': algo = KNNWithMeans()
  if df_benchmark.index[0] == 'KNNWithZScore': algo = KNNWithZScore()
  if df_benchmark.index[0] == 'BaselineOnly': algo = BaselineOnly()
  if df_benchmark.index[0] == 'CoClustering': algo = CoClustering()

  return algo

def main():
  # Load .env variables
  load_dotenv()
  try:
    # Create a MySQL connection
    db = connection.connect(host = os.getenv('MYSQL_HOST'), user = os.getenv('MYSQL_USER'), port = os.getenv('MYSQL_PORT'), 
      passwd =  os.getenv('MYSQL_PASSWORD'), database = os.getenv('MYSQL_DATABASE'), use_pure = True)
    # Fetch saved ratings
    query_ratings = "SELECT user_id, course_id, rating FROM users_courses"
    df_ratings = pd.read_sql(query_ratings, db)

    # Create a Surprise reader. The 0 ratings are not valid
    reader = Reader(rating_scale = (1, 5))
    data = Dataset.load_from_df(df_ratings[['user_id', 'course_id', 'rating']], reader)

    # Test the performance for all the available algorithms with the dataset and pick the best one
    algo = get_best_algorithm(data)
    # Cross validate the dataset with the selected algorithm
    cross_validate(algo, data, measures = ['RMSE'], cv = 3, verbose = True)

    # Train the algorithm with train/test sets and create the prediction
    trainset, testset = train_test_split(data, test_size = 0.20)
    predictions = algo.fit(trainset).test(testset)
    print("RMSE value for the predictions:", accuracy.rmse(predictions))

    # Now that we have the prediction matrix, we will get the top predictions for the users
    # We only perform recommendations to courses that have been rated at least once
    unique_courses = df_ratings['course_id'].unique()
    # And users that have voted at least once
    unique_users = df_ratings['user_id'].unique()

    # Iterate all the valid users
    for user_id in unique_users:
      print("\nPredicting courses for the user", user_id)
      # Take only the courses that we don't know the rating, discarding the already rated ones
      rated_courses = df_ratings.loc[df_ratings['user_id'] == user_id, 'course_id']
      courses_to_predict = np.setdiff1d(unique_courses, rated_courses)

      # Predict the ratings for each of the non-rated courses by the user
      user_predictions = []
      for course_id in courses_to_predict:
        prediction = algo.predict(uid = user_id, iid = course_id)
        user_predictions.append( { "course_id": course_id, "predicted_rating": prediction.est })

      # Get just the top 4 predictions for the user and sort by rating
      user_courses_predictions_df = pd.DataFrame(user_predictions)
      user_courses_top_predictions_df = user_courses_predictions_df.sort_values(by = 'predicted_rating', ascending = [False])[:4]
      print("Top predictions")
      print(user_courses_top_predictions_df)

      # Remove previously saved predictions
      cursor = db.cursor()
      cursor.execute("DELETE FROM users_courses_recommendations WHERE type = 0 AND user_id = %s", (int(user_id),))

      # Insert new user predictions
      for _, user_course_prediction in user_courses_top_predictions_df.iterrows():
        cursor.execute("INSERT INTO users_courses_recommendations (user_id, course_id, predicted_rating, type) VALUES(%s, %s, %s, 0)",
          (int(user_id), int(user_course_prediction['course_id']), float(user_course_prediction['predicted_rating'])))

    # Execute all the SQL write queries
    db.commit()
  except Exception as e:
    # Print any error occured
    print(str(e))
  finally:
    # Always close the db connection
    db.close()

if __name__ == "__main__":
  main()
