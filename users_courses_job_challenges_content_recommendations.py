import pandas as pd
from math import sqrt
from dotenv import load_dotenv
import mysql.connector as connection
import os

def main():
  # Load .env variables
  load_dotenv()
  try:
    # Create a MySQL connection
    db = connection.connect(host = os.getenv('MYSQL_HOST'), user = os.getenv('MYSQL_USER'), port = os.getenv('MYSQL_PORT'), 
      passwd =  os.getenv('MYSQL_PASSWORD'), database = os.getenv('MYSQL_DATABASE'), use_pure = True)

    # Get all the users that have set at least one job challenge
    query_users_with_challenges = "SELECT DISTINCT user_id FROM users_occupations"
    cursor = db.cursor()
    cursor.execute(query_users_with_challenges)

    # Iterate over all of them and predict the top 4 courses
    users_with_challenges_tuple = cursor.fetchall()
    for user_id in users_with_challenges_tuple:
      user_id = str(user_id[0])
      print("\nPredicting courses for the user", user_id)

      # Remove previously saved predictions
      cursor = db.cursor()
      cursor.execute("DELETE FROM users_courses_recommendations WHERE type = 1 AND user_id = %s", (int(user_id),))

      # Get the job challenges for an user
      query_user_challenges = "SELECT DISTINCT occupation_id FROM users_occupations WHERE user_id = " + user_id
      cursor = db.cursor()
      cursor.execute(query_user_challenges)
      # The database returns a tuple   
      user_challenges_tuple = cursor.fetchall()
      # We need to convert it to a dictionary to create a Pandas series
      user_challenges_dic = {}
      for user_challenge in user_challenges_tuple:
        # We need to save the ids as strings
        user_challenges_dic[str(user_challenge[0])] = 1
      # This series is the user profile
      user_challenges_series = pd.Series(data=user_challenges_dic)

      # We need to create a Pandas dataframe composed of the rest of users and its jobs challenges
      # Each row must contain the user id and if has the job challenge from the column
      # Example:
      #             4975  5213  5217  5504
      #  user_id
      #  83          1     0     0     0
      #  125         0     0     1     1
      #  133         0     0     0     0

      # Fetch the rest of users that have any job challenge set
      query_users_challenges = "SELECT user_id, occupation_id FROM users_occupations WHERE user_id != " + user_id
      cursor.execute(query_users_challenges)
      users_challenges_tuple = cursor.fetchall()

      # Create a table where each row is a user with its job challenges (1 or 0)
      users_challenges_table = []
      for user in users_challenges_tuple:
        row = {}
        row["user_id"] = user[0]
        # Check if the user has the job challenge
        for challenge in user_challenges_dic:
          # We use strings to make it compatible with the user profile list
          if str(user[1]) == challenge:
            row[str(challenge)] = 1
          else:
            row[str(challenge)] = 0
        users_challenges_table.append(row)

      # The table may contain duplicated users since they may have multiple job challenges
      # We need to aggregate them by the user_id column and sum the rows
      users_challenges_table_df = pd.DataFrame(users_challenges_table)
      users_challenges_table_agg_df = users_challenges_table_df.groupby(['user_id']).agg('sum')

      # With the input user profile and the rest of users profiles, we are going to take the weighted average
      # of every user based on the input profile and get the closest users to the input user
      closest_users_df = ((users_challenges_table_agg_df * user_challenges_series).sum(axis=1))/(user_challenges_series.sum())

      # Top 20 sorted by proximity (the higher the better)
      closest_users_sorted_df = closest_users_df.sort_values(ascending=False)[:20]

      # Get the top rated courses from the closest users and predict ratings for the courses
      user_predictions = []
      for similar_user_id, similarity in closest_users_sorted_df.items():
        # Discard similarity values of 0
        if similarity == 0: continue

        query_user_courses_ratings = "SELECT course_id, rating FROM users_courses WHERE user_id = " + str(similar_user_id)
        cursor.execute(query_user_courses_ratings)
        user_courses_ratings_tuple = cursor.fetchall()
        # For each rating, predict a rating with the similarity factor
        for couse_id, rating in user_courses_ratings_tuple:
          # We use the square root to make values near 1 closer to 1. Eg. sqrt(0.5) = 0.707
          predicted_rating = float(rating) * sqrt(similarity)
          user_predictions.append({ "course_id": couse_id, "predicted_rating": predicted_rating })

      # Skip if user_predictions is empty
      if not user_predictions: continue

      # Sort user predictions by the top 4 predicted rating, removing duplicated recommendations
      user_courses_predictions_df = pd.DataFrame(user_predictions).drop_duplicates()
      user_courses_top_predictions_df = user_courses_predictions_df.sort_values(by = 'predicted_rating', ascending = [False])[:4]
      print("Top predictions")
      print(user_courses_top_predictions_df)

      # Insert new user predictions
      for _, user_course_prediction in user_courses_top_predictions_df.iterrows():
        cursor.execute("INSERT INTO users_courses_recommendations (user_id, course_id, predicted_rating, type) VALUES(%s, %s, %s, 1)",
          (int(user_id), int(user_course_prediction['course_id']), float(user_course_prediction['predicted_rating'])))

    # Execute all the SQL write queries
    db.commit()
  except Exception as e:
    # Print any error occured
    print(str(e))
  finally:
    # Always close the db connection
    db.close()

if __name__ == "__main__":
  main()
