import pandas as pd
from math import sqrt
from dotenv import load_dotenv
import mysql.connector as connection
import os

def main():
  # Load .env variables
  load_dotenv()
  try:
    # Create a MySQL connection
    db = connection.connect(host = os.getenv('MYSQL_HOST'), user = os.getenv('MYSQL_USER'), port = os.getenv('MYSQL_PORT'), 
      passwd =  os.getenv('MYSQL_PASSWORD'), database = os.getenv('MYSQL_DATABASE'), use_pure = True)

    # Get all the users that have rated at least one course
    query_users_with_courses = "SELECT DISTINCT user_id FROM users_courses"
    cursor = db.cursor()
    cursor.execute(query_users_with_courses)

    # Iterate over all of them and predict the top 4 courses
    users_with_courses_tuple = cursor.fetchall()
    for user_id in users_with_courses_tuple:
      user_id = str(user_id[0])
      print("\nPredicting courses for the user", user_id)

      # Remove previously saved predictions
      cursor = db.cursor()
      cursor.execute("DELETE FROM users_courses_recommendations WHERE type = 2 AND user_id = %s", (int(user_id),))

      # Here will be stored the course predictions for the user
      course_predictions = []

      # Get the rated courses for an user
      query_user_courses = "SELECT DISTINCT course_id, rating FROM users_courses WHERE user_id = " + user_id
      cursor = db.cursor()
      cursor.execute(query_user_courses)
      # The database returns a tuple   
      user_courses_tuple = cursor.fetchall()
      # Iterate over all the courses that the user has rated
      for rated_course in user_courses_tuple:
        course_id = str(rated_course[0])

        # Get the course caracteristics for the course
        query_course_characteristics = "SELECT course_id, category_id, language_id, partner_id, keywords FROM courses WHERE course_id = " + course_id
        cursor = db.cursor()
        cursor.execute(query_course_characteristics)
        # The database returns a tuple   
        course_characteristics_tuple = cursor.fetchall()
        # We need to convert it to a dictionary to create a Pandas series
        course_characteristics_dic = {}
        # Map the characteristics into a dictionary
        if course_characteristics_tuple[0][1]:
          course_characteristics_dic["category_id_" + str(course_characteristics_tuple[0][1])] = 1
        if course_characteristics_tuple[0][2]:
          course_characteristics_dic["language_id_" + str(course_characteristics_tuple[0][2])] = 1
        if course_characteristics_tuple[0][3]:
          course_characteristics_dic["partner_id_" + str(course_characteristics_tuple[0][3])] = 1
        if course_characteristics_tuple[0][4]:
          keywords_array = [x.strip() for x in course_characteristics_tuple[0][4].split(',')]
          for keyword in keywords_array:
            course_characteristics_dic["keyword_" + keyword.lower()] = 1

        # This Pandas series is the course profile
        course_characteristics_series = pd.Series(data=course_characteristics_dic)

        # We need to create a Pandas dataframe composed of the rest of courses and its characteristics
        # Each row must contain the course id and if has the characteristic from the column
        # Example:
        #             category_id_5  type_id_13  language_id_107  partner_id_34
        #  course_id
        #  83               1             1              0              0
        #  125              1             0              1              1
        #  133              0             0              0              0

        # Fetch the rest of courses and its characteristics
        query_course_characteristics = "SELECT course_id, category_id, language_id, partner_id, keywords FROM courses WHERE course_id != " + course_id
        cursor.execute(query_course_characteristics)
        courses_characteristics_touple = cursor.fetchall()

        # Create a table where each row is a course with its characteristics (1 or 0)
        courses_characteristics_table = []
        for course in courses_characteristics_touple:
          row = {}
          row["course_id"] = course[0]

          # Check if the course has the characteristic in common with the input course
          if "category_id_" + str(course[1]) in course_characteristics_dic:
            row["category_id_" + str(course[1])] = 1
          else:
            row["category_id_" + str(course[1])] = 0
            
          if "language_id_" + str(course[2]) in course_characteristics_dic:
            row["language_id_" + str(course[2])] = 1
          else:
            row["language_id_" + str(course[2])] = 0

          if "partner_id_" + str(course[3]) in course_characteristics_dic:
            row["partner_id_" + str(course[3])] = 1
          else:
            row["partner_id_" + str(course[3])] = 0

          # We need to do this in order to detect the keywords and 
          # not to change the structure of the table
          for input_characteristic in course_characteristics_dic:
            if "keyword_" in input_characteristic:
              if course[4]:
                keyword_found = False
                keywords_array = [x.strip() for x in course[4].split(',')]
                for keyword in keywords_array:
                  if "keyword_" + keyword.lower() == input_characteristic:
                    keyword_found = True
                if keyword_found:
                  row[input_characteristic] = 1
                else:
                  row[input_characteristic] = 0                    

          # Save the course matches as a row in the table
          courses_characteristics_table.append(row)

        # Create dataframe from the courses table and group by course_id
        # It forces to set the course_id as the index so we can extract the best courses with its course_id
        courses_characteristics_table_df = pd.DataFrame(courses_characteristics_table)
        courses_characteristics_table_agg_df = courses_characteristics_table_df.groupby(['course_id']).agg('sum')

        # With the input course profile and the rest of courses profiles, we are going to take the weighted average
        # of every course based on the input profile and get the closest course to the input course
        closest_courses_df = ((courses_characteristics_table_agg_df * course_characteristics_series).sum(axis=1))/(course_characteristics_series.sum())

        # Top 20 sorted by proximity (the higher the better)
        closest_courses_sorted_df = closest_courses_df.sort_values(ascending=False)[:20]

        # Predict a course rating based on the rating of the course we are comparing
        # and the similarity between the rest of courses
        for similar_course_id, similarity in closest_courses_sorted_df.items():
          # Discard similarity values of 0
          if similarity == 0: continue

          # Get the rating from the course we are comparing
          rating = str(rated_course[1])

          # We use the square root to make values near 1 closer to 1. Eg. sqrt(0.5) = 0.707
          predicted_rating = float(rating) * sqrt(similarity)
          course_predictions.append({ "course_id": similar_course_id, "predicted_rating": predicted_rating })
      
      # Get the top 4 courses predictions for the user by the predicted rating
      user_courses_predictions_df = pd.DataFrame(course_predictions)
      user_courses_top_predictions_df = user_courses_predictions_df.sort_values(by = 'predicted_rating', ascending = [False])[:4]
      print("Top predictions")
      print(user_courses_top_predictions_df)

      # Insert new user predictions
      for _, user_course_prediction in user_courses_top_predictions_df.iterrows():
        cursor.execute("INSERT INTO users_courses_recommendations (user_id, course_id, predicted_rating, type) VALUES(%s, %s, %s, 2)",
          (int(user_id), int(user_course_prediction['course_id']), float(user_course_prediction['predicted_rating'])))

    # Execute all the SQL write queries
    db.commit()
  except Exception as e:
    # Print any error occured
    print(str(e))
  finally:
    # Always close the db connection
    db.close()

if __name__ == "__main__":
  main()
