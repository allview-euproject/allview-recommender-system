import pandas as pd
from dotenv import load_dotenv
import mysql.connector as connection
import os

def main():
  # Load .env variables
  load_dotenv()
  try:
    # Create a MySQL connection
    db = connection.connect(host = os.getenv('MYSQL_HOST'), user = os.getenv('MYSQL_USER'), port = os.getenv('MYSQL_PORT'), 
      passwd =  os.getenv('MYSQL_PASSWORD'), database = os.getenv('MYSQL_DATABASE'), use_pure = True)

    # Get all the users that are people type
    query_users = "SELECT user_id FROM users WHERE role_id = 1"
    cursor = db.cursor()
    cursor.execute(query_users)

    # Iterate over all of them and predict the top 4 jobs
    query_users_tuple = cursor.fetchall()
    for user in query_users_tuple:
      user_id = str(user[0])
      print("\nPredicting jobs for the user", user_id)

      # Remove previously saved predictions
      cursor = db.cursor()
      cursor.execute("DELETE FROM users_jobs_recommendations WHERE type = 0 AND user_id = %s", (int(user_id),))
      # Execute SQL transaction
      db.commit()

      # Get the user skills
      query_user_skills = "SELECT skill_id FROM users_skills WHERE user_id = " + user_id
      cursor = db.cursor()
      cursor.execute(query_user_skills)
      # The database returns a tuple   
      user_skills_tuple = cursor.fetchall()

      # Skip if the user has no skills selected
      if not user_skills_tuple: continue

      # We need to convert it to a dictionary to create a Pandas series
      user_skills_dic = {}
      for user_skill in user_skills_tuple:
        # We need to save the ids as strings
        user_skills_dic["skill_id_" + str(user_skill[0])] = 1
      # This series is the user profile
      user_skills_series = pd.Series(data=user_skills_dic)

      # We need to create a Pandas dataframe composed of the rest of courses and its characteristics
      # Each row must contain the course id and if has the characteristic from the column
      # Example:
      #              skill_id_5   skill_id_13   skill_id_107  skill_id_34
      #  job_id
      #  83               1             1              0              0
      #  125              1             0              1              1
      #  133              0             0              0              0

      # Fetch all the jobs and its skills
      query_jobs_skills = "SELECT job_id, skill_id FROM jobs_skills"
      cursor.execute(query_jobs_skills)
      jobs_skills_tuple = cursor.fetchall()

      # Create a table where each row is a job that contains or not the skill_id
      jobs_skills_table = []
      for job_skill in jobs_skills_tuple:
        row = {}
        row["job_id"] = job_skill[0]
        # Check if the job has the user skill
        for user_skill in user_skills_dic:
          # If the job contains the skill, set a 1 in the column
          if user_skill == "skill_id_" + str(job_skill[1]):
            row[user_skill] = 1
          else:
            row[user_skill] = 0
        jobs_skills_table.append(row)
      
      # The table may contain duplicated jobs since they may have multiple skills
      # We need to aggregate them by the job_id column and sum the rows
      jobs_skills_table_df = pd.DataFrame(jobs_skills_table)
      jobs_skills_table_agg_df = jobs_skills_table_df.groupby(['job_id']).agg('sum')

      # Take the weighted average of the proximity of all the courses relative to the input user profile
      closest_jobs_df = ((jobs_skills_table_agg_df * user_skills_series).sum(axis=1))/(user_skills_series.sum())

      # Top 4 jobs sorted by proximity (the higher the better)
      closest_jobs_sorted_df = closest_jobs_df.sort_values(ascending=False)[:4]
      print("Top job predictions")
      print(closest_jobs_sorted_df)

      # Insert jobs predictions
      for job_id, score in closest_jobs_sorted_df.items():
        cursor.execute("INSERT INTO users_jobs_recommendations (user_id, job_id, score, type) VALUES(%s, %s, %s, 0)",
          (int(user_id), int(job_id), float(score)))

    # Execute all the SQL write queries
    db.commit()
  except Exception as e:
    # Print any error occured
    print(str(e))
  finally:
    # Always close the db connection
    db.close()

if __name__ == "__main__":
  main()
